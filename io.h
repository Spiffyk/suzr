/*
 * Copyright (c) Oto Šťáva
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**@file
 * Common function library of `suzr`, with shared functionality for the CLI, as
 * well as the GUI.
 *
 * Many functions here work with strings, mainly for file names. These are
 * always assumed to be UTF-8, even if they at some point interface with the
 * underlying OS. The OS-specific implementations are responsible for proper
 * conversion into the OS-native format.
 */

#pragma once

#include "export.h"

SUZR_EXPORT_START()

#include <stdbool.h>
#include <stddef.h>

/** Function type for a file callback. `filename` is a full absolute path to a
 * file, `baton` is a user-specified pointer.
 *
 * Returns 0 on success, or an `errno` code on error. */
typedef int (*suzr_file_cb)(const char *filename, void *baton);

/** For each regular file under the directory `dirname` and - if `recurse` is
 * `true` - all its subdirectories, calls `file_cb`, passing the full absolute
 * file path and the provided `baton` to it. If `file_cb` returns an error, the
 * recursion stops.
 *
 * Returns 0 on success, or an error code on error. */
SUZR_PUBLIC
int suzr_io_recurse_directory(const char *dirname,
                              suzr_file_cb file_cb,
                              bool recurse,
                              void *baton);

/** Deletes the file with the specified `filename`. */
SUZR_PUBLIC
int suzr_io_delete(const char *filename);

/** Writes the full contents of the buffer `buf` of size `buf_size` into the
 * file with the specified `filename`.
 *
 * Returns 0 on success, or an error code on error. */
SUZR_PUBLIC
int suzr_io_write(const char *filename,
                  const void *buf,
                  size_t buf_size);

/** Reads the entire file with the specified `filename` into a newly `malloc`'d
 * memory buffer. Pointer to the buffer is written into `out`, size of the
 * buffer is written into `out_size`.
 *
 * Returns 0 on success, or an error code on error. */
SUZR_PUBLIC
int suzr_io_read(const char *filename, void **out, size_t *out_size);

/** Gets the specified `filename` relatively to the user directory of the
 * program and writes it into `buf` of size `buf_size`, NULL-terminated. On
 * Windows this will be somewhere in `AppData`, on POSIX this will be somewhere
 * in `~/.config`.
 *
 * Returns 0 on success, or an error code on error. `ENOBUFS` is returned when
 * the path is too long for the `buf_size`. */
SUZR_PUBLIC
int suzr_io_get_user_file(char *buf, size_t buf_size, const char *filename);

/** Makes sure that the user directory of the program exists.
 *
 * Returns 0 if the directory already existed or has been created, or an error
 * code on error. `ENOBUFS` is returned when the path to the user directory is
 * too long. */
SUZR_PUBLIC
int suzr_io_ensure_user_dir(void);

SUZR_EXPORT_END()
