/*
 * Copyright (c) Oto Šťáva
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "export.h"

#pragma once

SUZR_EXPORT_START();

#include <string.h>
#include <stddef.h>


// Logging /////////////////////////////////////////////////////////////////////

/** Prints a formatted message to `stderr`. */
#define ERRF(fmt, ...) fprintf(stderr, fmt, __VA_ARGS__)

/** A non-debug assertion - aborts the program if `assertion` is `false`. */
#define REQUIRE(assertion) do {\
	if (!(assertion)) {\
		ERRF("%s:%d: requirement failed: (%s)\n", \
				__FILE__, __LINE__, #assertion);\
		abort();\
	}\
} while (0)


// Utilities ///////////////////////////////////////////////////////////////////

/** Like `strncmp`, but both strings have their respective length supplied and
 * are not assumed to be null-terminated during pre-requisite check. */
static inline int suzr_strcmp(const char *a, size_t a_len, const char *b, size_t b_len)
{
	if (a_len > b_len)
		return 1;
	if (a_len < b_len)
		return -1;

	return strncmp(a, b, a_len);
}

/** Convenience macro to check if the supplied string `a` of length `a_len` is
 * equal to the constant string `const_b`. */
#define SUZR_CONST_STRCMP(a, a_len, const_b) \
	(suzr_strcmp((a), (a_len), (const_b), sizeof((const_b)) - 1))

/** Gets the number of elements in `arr`. `arr` MUST be a statically declared
 * array, otherwise unexpected results will come out of this. */
#define SUZR_ARRSIZEOF(arr) \
	(sizeof((arr)) / sizeof((arr)[0]))

SUZR_EXPORT_END();
