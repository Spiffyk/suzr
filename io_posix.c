/*
 * Copyright (c) 2023 Oto Šťáva
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <assert.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/stat.h>
#include <unistd.h>

#include "suzr.h"

#include "io.h"

int suzr_io_recurse_directory(const char *dirname,
                              suzr_file_cb file_cb,
                              bool recurse,
                              void *baton)
{
	int ret = 0;
	DIR *dir = opendir(dirname);
	if (!dir) {
		ret = errno;
		goto exit;
	}

	char oldpwd_buf[PATH_MAX];
	char *oldpwd = getcwd(oldpwd_buf, sizeof(oldpwd_buf));
	if (!oldpwd) {
		ret = errno;
		goto exit_dir;
	}

	ret = chdir(dirname);
	if (ret)
		goto exit_dir;

	struct dirent *ent;
	while ((ent = readdir(dir))) {
		if (ent->d_name[0] == '.')
			continue;

		char fullpath_buf[PATH_MAX];
		char *fullpath = realpath(ent->d_name, fullpath_buf);
		if (!fullpath) {
			ret = errno;
			goto exit_oldpwd;
		}

		ret = 0;
		if (ent->d_type == DT_DIR) {
			if (recurse) {
				ret = suzr_io_recurse_directory(fullpath, file_cb,
						recurse, baton);
			}
		} else if (ent->d_type == DT_REG) {
			ret = file_cb(fullpath, baton);
		}

		if (ret)
			goto exit_oldpwd;
	}

exit_oldpwd:
	chdir(oldpwd);
exit_dir:
	closedir(dir);
exit:
	return ret;
}

int suzr_io_delete(const char *filename)
{
	return unlink(filename);
}

int suzr_io_write(const char *filename,
               const void *buf,
               size_t buf_size)
{
	int fd = open(filename, O_CREAT | O_TRUNC | O_WRONLY, 0666);
	if (fd == -1)
		return errno;

	int ret = 0;
	ssize_t wrret = write(fd, buf, buf_size);
	if (wrret == -1)
		ret = errno;

	int clret = close(fd);
	if (!ret && clret)
		return clret;
	return ret;
}

int suzr_io_read(const char *filename, void **out, size_t *out_size)
{
	int fd = open(filename, O_RDONLY);
	if (fd == -1)
		return errno;

	int ret = 0;
	size_t cur_size = 0;
	size_t capacity = 4096;
	uint8_t *buf = malloc(capacity);
	if (!buf) {
		ret = ENOMEM;
		goto exit_close;
	}

	ssize_t rdret;
	do {
		if (capacity - cur_size == 0) {
			capacity *= 2;
			buf = realloc(buf, capacity);
			if (!buf) {
				ret = ENOMEM;
				goto exit_close;
			}
		}
		rdret = read(fd, &buf[cur_size], capacity - cur_size);
		if (rdret < 0) {
			ret = errno;
			goto exit_free;
		}

		cur_size += rdret;
	} while (rdret);

	*out = buf;
	*out_size = cur_size;
	ret = 0;
	goto exit_close;

exit_free:
	free(buf);
exit_close:;
	int clret = close(fd);
	if (!ret && clret)
		return clret;
	return ret;
}

int suzr_io_get_user_file(char *buf, size_t buf_size, const char *filename)
{
	char *env;
	int ret;
	if ((env = getenv("XDG_CONFIG_DIR"))) {
		ret = snprintf(buf, buf_size,
				"%s/" SUZR_USER_DIR_NAME "/%s",
				env, filename);
	} else if ((env = getenv("HOME"))) {
		ret = snprintf(buf, buf_size,
				"%s/.config/" SUZR_USER_DIR_NAME "/%s",
				env, filename);
	} else {
		return EIO;
	}

	if (ret < 0)
		return ret;
	if (ret >= buf_size)
		return ENOBUFS;
	return 0;
}

int suzr_io_ensure_user_dir(void)
{
	char user_dir[PATH_MAX];
	int ret;

	ret = suzr_io_get_user_file(user_dir, sizeof(user_dir), "");
	if (ret)
		return ret;

	struct stat s;
	ret = stat(user_dir, &s);
	if (!ret) {
		if (s.st_mode & S_IFDIR)
			return 0;

		return EEXIST;
	}

	if (errno != ENOENT)
		return errno;

	ret = mkdir(user_dir, 0777);
	if (ret)
		return errno;

	return 0;
}
