/*
 * Copyright (c) 2023 Oto Šťáva
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <atomic>
#include <cctype>
#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <mutex>

#ifdef _WIN32 //////////////////////////////////////////////////////////////////

#include <windows.h>
#include <direct.h>
#include <d3d12.h>
#include <dxgi1_4.h>
#include <tchar.h>

#include "imgui_impl_win32.h"
#include "imgui_impl_dx12.h"

#define PATH_MAX MAX_PATH

#ifdef _DEBUG
#define DX12_ENABLE_DEBUG_LAYER
#endif // _DEBUG

#ifdef DX12_ENABLE_DEBUG_LAYER
#include <dxgidebug.h>
#pragma comment(lib, "dxguid.lib")
#endif // DX12_ENABLE_DEBUG_LAYER

#else // Linux and other POSIX-compliant ///////////////////////////////////////

#include <dlfcn.h>
#include <limits.h>
#include <pthread.h>
#include <sys/stat.h>
#include <unistd.h>

#include "GLFW/glfw3.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl2.h"

#endif /////////////////////////////////////////////////////////////////////////


#include <nfd.h>

#include "imgui.h"

#include "suzr.h"
#include "io.h"
#include "util.h"

#define WIN_FONT "\\Fonts\\segoeui.ttf"
#define FONT_SIZE 17

static const ImWchar GLYPH_RANGES[] =
{
	0x0020, 0x00FF, // Basic Latin + Latin Supplement
	0x0100, 0x01FF, // Czech stuff
	0,
};


static constexpr size_t STATUS_BUFFER_SIZE = 512;

struct state_struct {
	bool exit_requested;
	bool show_imgui_demo_window;
	bool recurse;
	std::atomic_bool working;
	char path_buffer[512];

	struct suzr_stats stats;

	std::mutex status_write_mutex;
	char status_buffer_a[STATUS_BUFFER_SIZE];
	char status_buffer_b[STATUS_BUFFER_SIZE];
	std::atomic_bool showing_status_a;
	std::atomic_bool is_status_error;

	char config_imgui[PATH_MAX];
	char config_config[PATH_MAX];
};

struct state_struct state;


static void init_state(void)
{
	// Reinit state (jeez, C++)
	state.~state_struct();
	new(&state) struct state_struct;

	int ret = suzr_io_get_user_file(state.config_imgui, sizeof(state.config_imgui),
			"imgui.ini");
	REQUIRE(!ret && "IMGUI file error");

	ret = suzr_io_get_user_file(state.config_config, sizeof(state.config_config),
			"suzr.conf");
	REQUIRE(!ret && "Config file error");

}

static void format_status(bool error, const char *fmt, ...)
{
	std::unique_lock l(state.status_write_mutex);
	bool showing_status_a = std::atomic_load(&state.showing_status_a);
	char *buf = (showing_status_a)
			? state.status_buffer_b : state.status_buffer_a;

	va_list ap;
	va_start(ap, fmt);
	vsnprintf(buf, STATUS_BUFFER_SIZE, fmt, ap);
	va_end(ap);

	std::atomic_store(&state.is_status_error, error);
	std::atomic_store(&state.showing_status_a, !showing_status_a);
}


// Config parser/loader ////////////////////////////////////////////////////////

#define CONFIG_LAST_PATH_KEY "last_path"
#define CONFIG_RECURSE_KEY "recurse"

static int config_handle_bool(const char *value, size_t value_length,
                              bool *out_value)
{
	if (SUZR_CONST_STRCMP(value, value_length, "true") == 0
	    || SUZR_CONST_STRCMP(value, value_length, "1") == 0) {
		*out_value = true;
		return 0;
	}
	if (SUZR_CONST_STRCMP(value, value_length, "false") == 0
	    || SUZR_CONST_STRCMP(value, value_length, "0") == 0) {
		*out_value = false;
		return 0;
	}

	return EINVAL;
}

/** Handles the specified configuration value.
 *
 * Returns 0 on success.
 * Returns `ENOENT` when the `key` is unknown.
 * Returns `ENOBUFS` when the `value` is too long.
 * Returns `EINVAL` when the `value` is invalid for the `key`. */
static int config_handle_value(const char *key, size_t key_length,
                               const char *value, size_t value_length)
{
	if (SUZR_CONST_STRCMP(key, key_length, CONFIG_LAST_PATH_KEY) == 0) {
		if (value_length > sizeof(state.path_buffer))
			return ENOBUFS;
		strncpy(state.path_buffer, value, value_length);
		if (value_length < sizeof(state.path_buffer))
			state.path_buffer[value_length] = '\0';
		return 0;
	}
	if (SUZR_CONST_STRCMP(key, key_length, CONFIG_RECURSE_KEY) == 0) {
		return config_handle_bool(value, value_length, &state.recurse);
	}

	return ENOENT;
}

static int config_parse(const char *buf, size_t buf_len)
{
	size_t cur = 0;

	for (;;) {
		// Skip whitespace at the beginning of the line
		for (; cur < buf_len; cur++) {
			if (!isspace((int)buf[cur]))
				break;
		}

		// Valid EOF
		if (cur >= buf_len)
			return 0;

		// Handle key
		size_t key_beg = cur;
		for (; cur < buf_len; cur++) {
			if (isspace((int)buf[cur]) || buf[cur] == '=')
				break;
			if (!isalpha((int)buf[cur]) && buf[cur] != '_')
				return EINVAL;
		}

		// File must not end after the key
		if (cur >= buf_len)
			return EPIPE;

		size_t key_length = cur - key_beg;

		if (!key_length)
			return EINVAL;

		// Skip whitespace after key
		for (; cur < buf_len; cur++) {
			if (!isspace((int)buf[cur]))
				break;
		}

		if (cur >= buf_len)
			return EPIPE;

		// Check if '='
		if (buf[cur] != '=')
			return EINVAL;
		cur++;

		// Skip whitespace after '='
		for (; cur < buf_len; cur++) {
			if (!isspace((int)buf[cur]) || buf[cur] == '\n')
				break;
		}

		if (cur >= buf_len)
			return EPIPE;

		// Handle value
		size_t value_beg = cur;
		for (; cur < buf_len; cur++) {
			if (buf[cur] == '\n')
				break;
		}

		size_t vcur = cur;
		// Strip trailing whitespace
		while (vcur > value_beg && isspace((int)buf[vcur - 1]))
			vcur--;
		size_t value_length = vcur - value_beg;

		int value_ret = config_handle_value(&buf[key_beg], key_length,
				&buf[value_beg], value_length);
		if (value_ret)
			return value_ret;
		cur++;
	}
}

static int config_load(void)
{
	void *data;
	size_t data_len;

	int ret = suzr_io_read(state.config_config, &data, &data_len);
	if (ret == ENOENT)
		return 0;
	if (ret)
		return ret;

	ret = config_parse((const char *)data, data_len);
	if (ret)
		return ret;

	return 0;
}

static int config_store(void)
{
	FILE *out = fopen(state.config_config, "w");
	if (!out)
		return errno;

	fprintf(out, CONFIG_LAST_PATH_KEY " = %.*s\n",
			(int)strnlen(state.path_buffer, sizeof(state.path_buffer)),
			state.path_buffer);

	fprintf(out, CONFIG_RECURSE_KEY " = %s\n",
			(state.recurse) ? "true" : "false");

	int clret = fclose(out);
	if (clret)
		return errno;
	return 0;
}


// Allocating sprintf (GNU-like) ///////////////////////////////////////////////

int vxasprintf(char **strp, const char *fmt, va_list ap)
{
	va_list ap2;
	va_copy(ap2, ap);
	int len = vsnprintf(NULL, 0, fmt, ap);
	if (len <= 0) {
		*strp = NULL;
		return 0;
	}

	*strp = (char *)malloc(len + 1);
	REQUIRE(*strp);
	vsnprintf(*strp, len + 1, fmt, ap2);
	return len;
}

int xasprintf(char **strp, const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	int len = vxasprintf(strp, fmt, ap);
	va_end(ap);
	return len;
}


// Threads /////////////////////////////////////////////////////////////////////

struct recurse_async_ctx {
	const char *dirname;
	suzr_file_cb file_cb;
	bool recurse;
	void *baton;
};

static void recurse_async(const char *dirname,
                          suzr_file_cb file_cb,
                          bool recurse,
                          void *baton);

#ifdef _WIN32
static DWORD WINAPI recurse_routine(LPVOID param)
#else
static void *recurse_routine(void *param)
#endif
{
	struct recurse_async_ctx ctx = *(struct recurse_async_ctx *)param;
	free(param);

	std::atomic_store(&state.working, true);
	int ret = suzr_io_recurse_directory(ctx.dirname, ctx.file_cb, ctx.recurse,
			ctx.baton);
	if (ret) {
		format_status(true, "%s", strerror(ret));
	} else {
		if (ctx.file_cb == suzr_gen_func) {
			format_status(false, "Generated %zu files", state.stats.files_modified);
		} else if (ctx.file_cb == suzr_clean_func) {
			format_status(false, "Deleted %zu files", state.stats.files_modified);
		} else {
			format_status(false, "Done");
		}
	}
	std::atomic_store(&state.working, false);

#ifdef _WIN32
	return 0;
#else
	return NULL;
#endif
}

#ifdef _WIN32
static void recurse_async(const char *dirname,
                          suzr_file_cb file_cb,
                          bool recurse,
                          void *baton)
{
	struct recurse_async_ctx *ctx = (decltype(ctx))malloc(sizeof(*ctx));
	REQUIRE(ctx);
	*ctx = { dirname, file_cb, recurse, baton };

	HANDLE thread_hnd = CreateThread(NULL, 0, recurse_routine, ctx, 0, NULL);
	REQUIRE(thread_hnd);
	CloseHandle(thread_hnd); // detach
}
#else
static void recurse_async(const char *dirname,
                          suzr_file_cb file_cb,
                          bool recurse,
                          void *baton)
{
	struct recurse_async_ctx *ctx = (decltype(ctx))malloc(sizeof(*ctx));
	REQUIRE(ctx);
	*ctx = (struct recurse_async_ctx){ dirname, file_cb, recurse, baton };

	pthread_t thr;
	int ret = pthread_create(&thr, NULL, recurse_routine, ctx);
	REQUIRE(ret == 0);
	pthread_detach(thr);
}
#endif


// Main GUI implementation /////////////////////////////////////////////////////

static constexpr int DEFAULT_WIDTH = 800;
static constexpr int DEFAULT_HEIGHT = 300;
constexpr ImVec4 CLEAR_COLOR(.2f, .2f, .2f, 1.0f);

static int suzr_gui_init(void)
{
	int ret = suzr_io_ensure_user_dir();
	if (ret)
		return ret;

	init_state();

	ret = config_load();
	if (ret) {
		init_state();
		format_status(true, "Error in configuration file (using default state)");
	}

	return 0;
}

static void suzr_gui_deinit(void)
{
	int ret = config_store();
	if (ret)
		fprintf(stderr, "config_store: %s\n", strerror(ret));
}

static ImGuiIO &imgui_setup_context()
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO &io = ImGui::GetIO();
	io.ConfigFlags |= 0
		| ImGuiConfigFlags_NavEnableKeyboard
		;
	io.IniFilename = state.config_imgui;

	ImGui::StyleColorsLight();
	return io;
}

static void browse_path(void)
{
	nfdchar_t *folder = NULL;
	nfdresult_t result = NFD_PickFolder(&folder, NULL);
	if (folder) {
		if (result == NFD_OKAY) {
			strncpy(state.path_buffer, folder, sizeof(state.path_buffer));
			format_status(false, "");
		} else if (result == NFD_ERROR) {
			format_status(true, "Error while opening the picker");
		}
		NFD_FreePath(folder);
	}
}

static void show_main_window(int window_width, int window_height)
{
	ImGui::SetNextWindowSize({ (float)window_width, (float)window_height });
	ImGui::SetNextWindowPos({ 0, 0 });
	constexpr ImGuiWindowFlags mwflags = 0
		| ImGuiWindowFlags_NoBringToFrontOnFocus
		| ImGuiWindowFlags_NoCollapse
		| ImGuiWindowFlags_NoMove
		| ImGuiWindowFlags_NoResize
		| ImGuiWindowFlags_NoTitleBar
      	;
	if (ImGui::Begin("suzr", nullptr, mwflags)) {
		bool working = std::atomic_load(&state.working);
		if (working)
			ImGui::BeginDisabled();

		ImGui::InputText("##Path",
				state.path_buffer, sizeof(state.path_buffer));
		ImGui::SameLine();
		if (ImGui::Button("Pick directory...##browse_button"))
			browse_path();

		ImGui::Spacing();

		ImGui::Checkbox("Recurse into subdirectories", &state.recurse);

		ImGui::Spacing();

		bool button_down = ImGui::Button("Generate BMPs");
		ImGui::SetItemTooltip("Generates files with the suffix "
				"'" SUZR_OUT_SUFFIX "' out of PSD files in the "
				"specified directory");
		if (button_down) {
			format_status(false, "Generating...");
			state.stats = {0};
			recurse_async(state.path_buffer, suzr_gen_func,
					state.recurse, &state.stats);
		}

		ImGui::SameLine();

		button_down = ImGui::Button("Delete BMPs");
		ImGui::SetItemTooltip("Deletes files with the suffix "
				"'" SUZR_OUT_SUFFIX "' from the specified "
				"directory");
		if (button_down) {
			format_status(false, "Deleting...");
			state.stats = {0};
			recurse_async(state.path_buffer, suzr_clean_func,
					state.recurse, &state.stats);
		}

		if (working)
			ImGui::EndDisabled();

		const char *status = std::atomic_load(&state.showing_status_a)
				? state.status_buffer_a : state.status_buffer_b;
		if (status && status[0]) {
			ImVec4 color = (std::atomic_load(&state.is_status_error))
				? ImVec4(1.0, 0.0, 0.0, 1.0)
				: ImGui::GetStyle().Colors[ImGuiCol_Text];
			ImGui::TextColored(color, "%s", status);
		}

		ImGui::End();
	}
}


#ifdef _WIN32

// Windows stuff ///////////////////////////////////////////////////////////////

struct FrameContext
{
	ID3D12CommandAllocator* CommandAllocator;
	UINT64                  FenceValue;
};

// Data
static int const                    NUM_FRAMES_IN_FLIGHT = 3;
static FrameContext                 g_frameContext[NUM_FRAMES_IN_FLIGHT] = {};
static UINT                         g_frameIndex = 0;

static int const                    NUM_BACK_BUFFERS = 3;
static ID3D12Device*                g_pd3dDevice = nullptr;
static ID3D12DescriptorHeap*        g_pd3dRtvDescHeap = nullptr;
static ID3D12DescriptorHeap*        g_pd3dSrvDescHeap = nullptr;
static ID3D12CommandQueue*          g_pd3dCommandQueue = nullptr;
static ID3D12GraphicsCommandList*   g_pd3dCommandList = nullptr;
static ID3D12Fence*                 g_fence = nullptr;
static HANDLE                       g_fenceEvent = nullptr;
static UINT64                       g_fenceLastSignaledValue = 0;
static IDXGISwapChain3*             g_pSwapChain = nullptr;
static HANDLE                       g_hSwapChainWaitableObject = nullptr;
static ID3D12Resource*              g_mainRenderTargetResource[NUM_BACK_BUFFERS] = {};
static D3D12_CPU_DESCRIPTOR_HANDLE  g_mainRenderTargetDescriptor[NUM_BACK_BUFFERS] = {};

// Helper functions
bool CreateDeviceD3D(HWND hWnd);
void CleanupDeviceD3D();
void CreateRenderTarget();
void CleanupRenderTarget();
void WaitForLastSubmittedFrame();
FrameContext* WaitForNextFrameResources();
LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

bool CreateDeviceD3D(HWND hWnd)
{
	// Setup swap chain
	DXGI_SWAP_CHAIN_DESC1 sd;
	{
		ZeroMemory(&sd, sizeof(sd));
		sd.BufferCount = NUM_BACK_BUFFERS;
		sd.Width = 0;
		sd.Height = 0;
		sd.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		sd.Flags = DXGI_SWAP_CHAIN_FLAG_FRAME_LATENCY_WAITABLE_OBJECT;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
		sd.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
		sd.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
		sd.Scaling = DXGI_SCALING_STRETCH;
		sd.Stereo = FALSE;
	}

	// [DEBUG] Enable debug interface
#ifdef DX12_ENABLE_DEBUG_LAYER
	ID3D12Debug* pdx12Debug = nullptr;
	if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&pdx12Debug))))
		pdx12Debug->EnableDebugLayer();
#endif

	// Create device
	D3D_FEATURE_LEVEL featureLevel = D3D_FEATURE_LEVEL_11_0;
	if (D3D12CreateDevice(nullptr, featureLevel, IID_PPV_ARGS(&g_pd3dDevice)) != S_OK)
		return false;

	// [DEBUG] Setup debug interface to break on any warnings/errors
#ifdef DX12_ENABLE_DEBUG_LAYER
	if (pdx12Debug != nullptr)
	{
		ID3D12InfoQueue* pInfoQueue = nullptr;
		g_pd3dDevice->QueryInterface(IID_PPV_ARGS(&pInfoQueue));
		pInfoQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_ERROR, true);
		pInfoQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_CORRUPTION, true);
		pInfoQueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_WARNING, true);
		pInfoQueue->Release();
		pdx12Debug->Release();
	}
#endif

	{
		D3D12_DESCRIPTOR_HEAP_DESC desc = {};
		desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
		desc.NumDescriptors = NUM_BACK_BUFFERS;
		desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
		desc.NodeMask = 1;
		if (g_pd3dDevice->CreateDescriptorHeap(&desc, IID_PPV_ARGS(&g_pd3dRtvDescHeap)) != S_OK)
			return false;

		SIZE_T rtvDescriptorSize = g_pd3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
		D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle = g_pd3dRtvDescHeap->GetCPUDescriptorHandleForHeapStart();
		for (UINT i = 0; i < NUM_BACK_BUFFERS; i++)
		{
			g_mainRenderTargetDescriptor[i] = rtvHandle;
			rtvHandle.ptr += rtvDescriptorSize;
		}
	}

	{
		D3D12_DESCRIPTOR_HEAP_DESC desc = {};
		desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
		desc.NumDescriptors = 1;
		desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
		if (g_pd3dDevice->CreateDescriptorHeap(&desc, IID_PPV_ARGS(&g_pd3dSrvDescHeap)) != S_OK)
			return false;
	}

	{
		D3D12_COMMAND_QUEUE_DESC desc = {};
		desc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
		desc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
		desc.NodeMask = 1;
		if (g_pd3dDevice->CreateCommandQueue(&desc, IID_PPV_ARGS(&g_pd3dCommandQueue)) != S_OK)
			return false;
	}

	for (UINT i = 0; i < NUM_FRAMES_IN_FLIGHT; i++)
		if (g_pd3dDevice->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&g_frameContext[i].CommandAllocator)) != S_OK)
			return false;

	if (g_pd3dDevice->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, g_frameContext[0].CommandAllocator, nullptr, IID_PPV_ARGS(&g_pd3dCommandList)) != S_OK ||
			g_pd3dCommandList->Close() != S_OK)
		return false;

	if (g_pd3dDevice->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&g_fence)) != S_OK)
		return false;

	g_fenceEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);
	if (g_fenceEvent == nullptr)
		return false;

	{
		IDXGIFactory4* dxgiFactory = nullptr;
		IDXGISwapChain1* swapChain1 = nullptr;
		if (CreateDXGIFactory1(IID_PPV_ARGS(&dxgiFactory)) != S_OK)
			return false;
		if (dxgiFactory->CreateSwapChainForHwnd(g_pd3dCommandQueue, hWnd, &sd, nullptr, nullptr, &swapChain1) != S_OK)
			return false;
		if (swapChain1->QueryInterface(IID_PPV_ARGS(&g_pSwapChain)) != S_OK)
			return false;
		swapChain1->Release();
		dxgiFactory->Release();
		g_pSwapChain->SetMaximumFrameLatency(NUM_BACK_BUFFERS);
		g_hSwapChainWaitableObject = g_pSwapChain->GetFrameLatencyWaitableObject();
	}

	CreateRenderTarget();
	return true;
}

void CleanupDeviceD3D()
{
	CleanupRenderTarget();
	if (g_pSwapChain) { g_pSwapChain->SetFullscreenState(false, nullptr); g_pSwapChain->Release(); g_pSwapChain = nullptr; }
	if (g_hSwapChainWaitableObject != nullptr) { CloseHandle(g_hSwapChainWaitableObject); }
	for (UINT i = 0; i < NUM_FRAMES_IN_FLIGHT; i++)
		if (g_frameContext[i].CommandAllocator) { g_frameContext[i].CommandAllocator->Release(); g_frameContext[i].CommandAllocator = nullptr; }
	if (g_pd3dCommandQueue) { g_pd3dCommandQueue->Release(); g_pd3dCommandQueue = nullptr; }
	if (g_pd3dCommandList) { g_pd3dCommandList->Release(); g_pd3dCommandList = nullptr; }
	if (g_pd3dRtvDescHeap) { g_pd3dRtvDescHeap->Release(); g_pd3dRtvDescHeap = nullptr; }
	if (g_pd3dSrvDescHeap) { g_pd3dSrvDescHeap->Release(); g_pd3dSrvDescHeap = nullptr; }
	if (g_fence) { g_fence->Release(); g_fence = nullptr; }
	if (g_fenceEvent) { CloseHandle(g_fenceEvent); g_fenceEvent = nullptr; }
	if (g_pd3dDevice) { g_pd3dDevice->Release(); g_pd3dDevice = nullptr; }

#ifdef DX12_ENABLE_DEBUG_LAYER
	IDXGIDebug1* pDebug = nullptr;
	if (SUCCEEDED(DXGIGetDebugInterface1(0, IID_PPV_ARGS(&pDebug))))
	{
		pDebug->ReportLiveObjects(DXGI_DEBUG_ALL, DXGI_DEBUG_RLO_SUMMARY);
		pDebug->Release();
	}
#endif
}

void CreateRenderTarget()
{
	for (UINT i = 0; i < NUM_BACK_BUFFERS; i++)
	{
		ID3D12Resource* pBackBuffer = nullptr;
		g_pSwapChain->GetBuffer(i, IID_PPV_ARGS(&pBackBuffer));
		g_pd3dDevice->CreateRenderTargetView(pBackBuffer, nullptr, g_mainRenderTargetDescriptor[i]);
		g_mainRenderTargetResource[i] = pBackBuffer;
	}
}

void CleanupRenderTarget()
{
	WaitForLastSubmittedFrame();

	for (UINT i = 0; i < NUM_BACK_BUFFERS; i++)
		if (g_mainRenderTargetResource[i]) { g_mainRenderTargetResource[i]->Release(); g_mainRenderTargetResource[i] = nullptr; }
}

void WaitForLastSubmittedFrame()
{
	FrameContext* frameCtx = &g_frameContext[g_frameIndex % NUM_FRAMES_IN_FLIGHT];

	UINT64 fenceValue = frameCtx->FenceValue;
	if (fenceValue == 0)
		return; // No fence was signaled

	frameCtx->FenceValue = 0;
	if (g_fence->GetCompletedValue() >= fenceValue)
		return;

	g_fence->SetEventOnCompletion(fenceValue, g_fenceEvent);
	WaitForSingleObject(g_fenceEvent, INFINITE);
}

FrameContext* WaitForNextFrameResources()
{
	UINT nextFrameIndex = g_frameIndex + 1;
	g_frameIndex = nextFrameIndex;

	HANDLE waitableObjects[] = { g_hSwapChainWaitableObject, nullptr };
	DWORD numWaitableObjects = 1;

	FrameContext* frameCtx = &g_frameContext[nextFrameIndex % NUM_FRAMES_IN_FLIGHT];
	UINT64 fenceValue = frameCtx->FenceValue;
	if (fenceValue != 0) // means no fence was signaled
	{
		frameCtx->FenceValue = 0;
		g_fence->SetEventOnCompletion(fenceValue, g_fenceEvent);
		waitableObjects[1] = g_fenceEvent;
		numWaitableObjects = 2;
	}

	WaitForMultipleObjects(numWaitableObjects, waitableObjects, TRUE, INFINITE);

	return frameCtx;
}

// Forward declare message handler from imgui_impl_win32.cpp
extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

// Win32 message handler
// You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
// - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application, or clear/overwrite your copy of the mouse data.
// - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application, or clear/overwrite your copy of the keyboard data.
// Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if (ImGui_ImplWin32_WndProcHandler(hWnd, msg, wParam, lParam))
		return true;

	switch (msg)
	{
	case WM_SIZE:
		if (g_pd3dDevice != nullptr && wParam != SIZE_MINIMIZED)
		{
			WaitForLastSubmittedFrame();
			CleanupRenderTarget();
			HRESULT result = g_pSwapChain->ResizeBuffers(0, (UINT)LOWORD(lParam), (UINT)HIWORD(lParam), DXGI_FORMAT_UNKNOWN, DXGI_SWAP_CHAIN_FLAG_FRAME_LATENCY_WAITABLE_OBJECT);
			assert(SUCCEEDED(result) && "Failed to resize swapchain.");
			CreateRenderTarget();
		}
		return 0;
	case WM_SYSCOMMAND:
		if ((wParam & 0xfff0) == SC_KEYMENU) // Disable ALT application menu
			return 0;
		break;
	case WM_DESTROY:
		::PostQuitMessage(0);
		return 0;
	}
	return ::DefWindowProcW(hWnd, msg, wParam, lParam);
}

static void win_handle_font(ImGuiIO& io)
{
	char font_path[MAX_PATH];
	DWORD sz = ::GetEnvironmentVariable("SYSTEMROOT", font_path, sizeof(font_path));
	if (!sz || sz >= MAX_PATH) {
		return;
	}
	errno_t err = strncat_s(font_path, MAX_PATH, WIN_FONT, sizeof(WIN_FONT));
	if (err) {
		return;
	}

	ImFontConfig fcfg = {};
	fcfg.OversampleH = fcfg.OversampleV = 3;
	io.Fonts->AddFontFromFileTTF(font_path, FONT_SIZE, &fcfg, GLYPH_RANGES);
}

// Windows entry point
int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine,
		int nShowCmd)
{
	int ret = suzr_gui_init();
	if (ret) {
		fprintf(stderr, "suzr_gui_init: %s\n", strerror(ret));
		return EXIT_FAILURE;
	}

	NFD_Init();

	// Create application window
	//ImGui_ImplWin32_EnableDpiAwareness();
	WNDCLASSEXW wc = { sizeof(wc), CS_CLASSDC, WndProc, 0L, 0L, GetModuleHandle(nullptr), nullptr, nullptr, nullptr, nullptr, L"suzr", nullptr };
	::RegisterClassExW(&wc);
	HWND hwnd = ::CreateWindowW(wc.lpszClassName, L"suzr " SUZR_VERSION,
			WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT, CW_USEDEFAULT,
			DEFAULT_WIDTH, DEFAULT_HEIGHT,
			nullptr, nullptr, wc.hInstance, nullptr);

	// Initialize Direct3D
	if (!CreateDeviceD3D(hwnd))
	{
		CleanupDeviceD3D();
		::UnregisterClassW(wc.lpszClassName, wc.hInstance);
		return 1;
	}

	// Show the window
	::ShowWindow(hwnd, SW_SHOWDEFAULT);
	::UpdateWindow(hwnd);

	ImGuiIO& io = imgui_setup_context();
	win_handle_font(io);

	// Setup Platform/Renderer backends
	ImGui_ImplWin32_Init(hwnd);
	ImGui_ImplDX12_Init(g_pd3dDevice, NUM_FRAMES_IN_FLIGHT,
			DXGI_FORMAT_R8G8B8A8_UNORM, g_pd3dSrvDescHeap,
			g_pd3dSrvDescHeap->GetCPUDescriptorHandleForHeapStart(),
			g_pd3dSrvDescHeap->GetGPUDescriptorHandleForHeapStart());

	// Load Fonts
	// - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
	// - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
	// - If the file cannot be loaded, the function will return a nullptr. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
	// - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
	// - Use '#define IMGUI_ENABLE_FREETYPE' in your imconfig file to use Freetype for higher quality font rendering.
	// - Read 'docs/FONTS.md' for more instructions and details.
	// - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
	//io.Fonts->AddFontDefault();
	//io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\segoeui.ttf", 18.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
	//io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
	//ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, nullptr, io.Fonts->GetGlyphRangesJapanese());
	//IM_ASSERT(font != nullptr);

	// Our state
	bool show_demo_window = true;
	bool show_another_window = false;

	// Main loop
	while (!state.exit_requested)
	{
		// Poll and handle messages (inputs, window resize, etc.)
		// See the WndProc() function below for our to dispatch events to the Win32 backend.
		MSG msg;
		while (::PeekMessage(&msg, nullptr, 0U, 0U, PM_REMOVE))
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
			if (msg.message == WM_QUIT)
				state.exit_requested = true;
		}
		if (state.exit_requested)
			break;

		// Start the Dear ImGui frame
		ImGui_ImplDX12_NewFrame();
		ImGui_ImplWin32_NewFrame();
		ImGui::NewFrame();

		RECT ws;
		bool ws_success = GetWindowRect(hwnd, &ws);
		assert(ws_success);
		show_main_window(ws.right - ws.left, ws.bottom - ws.top);

		if (state.show_imgui_demo_window)
			ImGui::ShowDemoWindow(&state.show_imgui_demo_window);

		// Rendering
		ImGui::Render();

		FrameContext* frameCtx = WaitForNextFrameResources();
		UINT backBufferIdx = g_pSwapChain->GetCurrentBackBufferIndex();
		frameCtx->CommandAllocator->Reset();

		D3D12_RESOURCE_BARRIER barrier = {};
		barrier.Type                   = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
		barrier.Flags                  = D3D12_RESOURCE_BARRIER_FLAG_NONE;
		barrier.Transition.pResource   = g_mainRenderTargetResource[backBufferIdx];
		barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
		barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
		barrier.Transition.StateAfter  = D3D12_RESOURCE_STATE_RENDER_TARGET;
		g_pd3dCommandList->Reset(frameCtx->CommandAllocator, nullptr);
		g_pd3dCommandList->ResourceBarrier(1, &barrier);

		// Render Dear ImGui graphics
		const float clear_color_with_alpha[4] = { CLEAR_COLOR.x * CLEAR_COLOR.w, CLEAR_COLOR.y * CLEAR_COLOR.w, CLEAR_COLOR.z * CLEAR_COLOR.w, CLEAR_COLOR.w };
		g_pd3dCommandList->ClearRenderTargetView(g_mainRenderTargetDescriptor[backBufferIdx], clear_color_with_alpha, 0, nullptr);
		g_pd3dCommandList->OMSetRenderTargets(1, &g_mainRenderTargetDescriptor[backBufferIdx], FALSE, nullptr);
		g_pd3dCommandList->SetDescriptorHeaps(1, &g_pd3dSrvDescHeap);
		ImGui_ImplDX12_RenderDrawData(ImGui::GetDrawData(), g_pd3dCommandList);
		barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
		barrier.Transition.StateAfter  = D3D12_RESOURCE_STATE_PRESENT;
		g_pd3dCommandList->ResourceBarrier(1, &barrier);
		g_pd3dCommandList->Close();

		g_pd3dCommandQueue->ExecuteCommandLists(1, (ID3D12CommandList* const*)&g_pd3dCommandList);

		g_pSwapChain->Present(1, 0); // Present with vsync
					     //g_pSwapChain->Present(0, 0); // Present without vsync

		UINT64 fenceValue = g_fenceLastSignaledValue + 1;
		g_pd3dCommandQueue->Signal(g_fence, fenceValue);
		g_fenceLastSignaledValue = fenceValue;
		frameCtx->FenceValue = fenceValue;
	}

	WaitForLastSubmittedFrame();

	// Cleanup
	ImGui_ImplDX12_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();

	CleanupDeviceD3D();
	::DestroyWindow(hwnd);
	::UnregisterClassW(wc.lpszClassName, wc.hInstance);

	NFD_Quit();
	suzr_gui_deinit();

	return 0;
}
#else

// Linux stuff /////////////////////////////////////////////////////////////////

static const char* LINUX_FONTS[] = {
	"/usr/share/fonts/TTF/DejaVuSans.ttf",
	"/usr/share/fonts/noto/NotoSans-Regular.ttf",
	"/usr/share/fonts/liberation/LiberationSans-Regular.ttf",
	NULL
};

static void unix_handle_font(ImGuiIO& io)
{
	for (size_t i = 0; LINUX_FONTS[i] != NULL; i++) {
		if (access(LINUX_FONTS[i], F_OK) != 0)
			return;

		ImFontConfig fcfg = {};
		fcfg.OversampleH = fcfg.OversampleV = 3;
		io.Fonts->AddFontFromFileTTF(LINUX_FONTS[i], FONT_SIZE, &fcfg, GLYPH_RANGES);
	}
}

static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "GLFW Error %d: %s\n", error, description);
}

int main()
{
	int ret = suzr_gui_init();
	if (ret) {
		fprintf(stderr, "suzr_gui_init: %s\n", strerror(ret));
		return EXIT_FAILURE;
	}

	NFD_Init();

	glfwSetErrorCallback(glfw_error_callback);
	if (glfwInit() == GLFW_FALSE)
		return EXIT_FAILURE;

	glfwWindowHintString(GLFW_WAYLAND_APP_ID, "suzr-gui");
	GLFWwindow *window = glfwCreateWindow(DEFAULT_WIDTH, DEFAULT_HEIGHT,
			"suzr " SUZR_VERSION, nullptr, nullptr);
	if (!window)
		return EXIT_FAILURE;
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);

	ImGuiIO& io = imgui_setup_context();
	unix_handle_font(io);

	// Backends
	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL2_Init();

	while (!glfwWindowShouldClose(window)) {
		glfwPollEvents();
		ImGui_ImplOpenGL2_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		int window_width, window_height;
		glfwGetWindowSize(window, &window_width, &window_height);
		show_main_window(window_width, window_height);

		if (state.show_imgui_demo_window)
			ImGui::ShowDemoWindow(&state.show_imgui_demo_window);

		ImGui::Render();

		glViewport(0, 0, (int)io.DisplaySize.x, (int)io.DisplaySize.y);
		glClearColor(CLEAR_COLOR.x * CLEAR_COLOR.w, CLEAR_COLOR.y * CLEAR_COLOR.w, CLEAR_COLOR.z * CLEAR_COLOR.w, CLEAR_COLOR.w);
		glClear(GL_COLOR_BUFFER_BIT);
		ImGui_ImplOpenGL2_RenderDrawData(ImGui::GetDrawData());
		glfwMakeContextCurrent(window);
		glfwSwapBuffers(window);
	}

	// Cleanup
	ImGui_ImplOpenGL2_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	glfwDestroyWindow(window);
	glfwTerminate();

	NFD_Quit();
	suzr_gui_deinit();

	return EXIT_SUCCESS;
}
#endif
