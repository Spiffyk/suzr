# Suzanne's PSD recursor

![A screenshot of suzr-gui](img/suzr-gui.png)

A simple program that recursively descends into a given directory structure and
creates BMP previews of all PSD files it finds there. Comes in the form of two
command line interface programs and a simple graphical utility.

Written with love for my Ace Suzanne ❤️


## Build

### Requirements

 * CMake 3.0 or higher
 * An ISO C99-compliant compiler
 * **For the GUI** (may be disabled using `-DGUI=0` while configuring the CMake
   project):
   * An ISO C++20-compliant compiler
   * **On Linux:**
     * SDL2
     * GTK3
     * OpenGL 2
   * **On Windows:**
     * DirectX 12

### Commands

```
$ mkdir build && cd build
$ cmake ..
$ cmake --build .
```

## Usage

The project contains three programs - `suzr-gen`, `suzr-clean`, and `suzr-gui`.

The former two are command line programs and simply take a list of directories
as their arguments. The latter one is a graphical program.

### `suzr-gen`

**Synopsis:** `suzr-gen [-V|--version] [-r|--recurse] directories...`

Finds all `.psd` files in the specified directories and generates `.suzr.bmp`
files out of them. Adding a `--recurse` or `-r` option causes it to also descend
into subdirectories.

Calling with the `--version` or `-V` makes the program print out its version and
exit without doing anything else.

### `suzr-clean`

**Synopsis:** `suzr-clean [-V|--version] [-r|--recurse] directories...`

Deletes all `.suzr.bmp` files in the specified directory. Adding a `--recurse`
or `-r` option causes it to also descend into subdirectories.

Calling with the `--version` or `-V` makes the program print out its version and
exit without doing anything else.

### `suzr-gui`

**Synopsis:** `suzr-gui`

A graphical program that allows the user to do the above operations using a
simple graphical user interface. The mode of operation (i.e. BMP generation or
deletion) is handled by two buttons. Recursion is handled by a checkbox.

All options are remembered between sessions.


## Technical notes

 * The `suzr` part of the `.suzr.bmp` file name is only there for `suzr-clean`
   to recognize the files created by `suzr-gen`, so that it does not delete any
   other potential BMP files the user might have laying around in the directory
   structure. It does not have any special meaning regarding the image format.
   The BMP images are standard Microsoft bitmaps using a `BITMAPV5HEADER`. Alpha
   channel is supported - transparent PSDs will produce transparent BMPs.

 * Tested on Manjaro Linux and Windows 10. Should also work on any
   POSIX-compliant OS (like MacOS?).


## Acknowledgements

The project uses `stb_image.h` from the
[*stb library* by Sean Barrett](https://github.com/nothings/stb) to load flattened
images from PSD files. A big thanks to Sean for making this little project
possible!

The `suzr-gui` program uses
[*Native File Dialog - Extended* by Bernard Teo and Michael Labbe](https://github.com/btzy/nativefiledialog-extended).
A big thanks to them as well, since I really could not be bothered with the
Win32 API for file dialogs when I saw what a mess it would be.
