/*
 * Copyright (c) Oto Šťáva
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/**@file
 * Common function library of `suzr`, with shared functionality for the CLI, as
 * well as the GUI.
 *
 * Many functions here work with strings, mainly for file names. These are
 * always assumed to be UTF-8, even if they at some point interface with the
 * underlying OS. The OS-specific implementations are responsible for proper
 * conversion into the OS-native format.
 */

#pragma once

#include "export.h"

SUZR_EXPORT_START()

#include <stdbool.h>
#include <stddef.h>


// Constants ///////////////////////////////////////////////////////////////////

#define SUZR_USER_DIR_NAME "suzr"
#define SUZR_IN_SUFFIX ".psd"
#define SUZR_OUT_SUFFIX ".suzr.bmp"


// Config //////////////////////////////////////////////////////////////////////

struct suzr_config {
	bool version;
	bool recurse;
	unsigned int num_files;
	const char **files;
};

/** Parses program arguments defined by `argc` and `argv`, emitting a
 * configuration.
 *
 * On success, returns 0 and writes the parsed config into `out_config`. On
 * failure, returns `EINVAL`. */
SUZR_PUBLIC
int suzr_config_argparse(int argc, char **argv, struct suzr_config *out_config);

/** Frees up the resources used by `config` (except for the struct itself, which
 * may be stack-allocated. */
SUZR_PUBLIC
void suzr_config_deinit(struct suzr_config *config);

/** Returns true if the null-terminated string `str` of maximum size
 * `str_buflen` ends with the specified `suffix`. */
SUZR_PUBLIC
bool suzr_str_ends_with(const char *str, size_t str_buflen, const char *suffix);

/** If null-terminated string `str` of maximum size `str_buflen` ends with
 * `old_suffix`, replaces that suffix with `new_suffix`.
 *
 * Returns 0 on success and a non-zero value on failure:
 *
 * - `EINVAL`: `old_suffix` has zero length
 * - `ERANGE`: `old_suffix` not found at the end of `str`
 * - `ENOBUFS`: string with replaced suffix won't fit `str_buflen` */
SUZR_PUBLIC
int suzr_str_replace_suffix(char *str, size_t str_buflen,
                            const char *old_suffix, const char *new_suffix);

/** Prints suzr version number into stdout in a unified way. */
SUZR_PUBLIC
void suzr_print_version(void);


// Recursion callbacks /////////////////////////////////////////////////////////

struct suzr_stats {
	size_t files_modified;
};

SUZR_PUBLIC
int suzr_gen_func(const char *in_filename, void *baton);

SUZR_PUBLIC
int suzr_clean_func(const char *filename, void *baton);

SUZR_EXPORT_END()
