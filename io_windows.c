#include <windows.h>
#include <fileapi.h>
#include <intsafe.h>

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <wchar.h>

#define SUZR_EXPORTS
#include "suzr.h"
#include "util.h"

#include "io.h"

/** Converts Windows last error into a POSIX equivalent. Mostly, this will be
 * EIO, but some special errors will be converted into something nice. */
static int suzr_io_win_last_error(void)
{
	DWORD err = GetLastError();
	switch (err) {
	case ERROR_ALREADY_EXISTS:         return EEXIST;
	case ERROR_FILE_NOT_FOUND:         return ENOENT;
	case ERROR_NO_UNICODE_TRANSLATION: return EINVAL;
	case ERROR_INSUFFICIENT_BUFFER:    return ENOBUFS;
	default: return EIO;
	}
}

SUZR_PUBLIC
int suzr_io_recurse_directory(const char *dirname,
                              suzr_file_cb file_cb,
                              bool recurse,
                              void *baton)
{
	int ret = 0;
	WIN32_FIND_DATAW ffd;

	wchar_t dirname_w[MAX_PATH];
	int conv_ret = MultiByteToWideChar(CP_UTF8, MB_ERR_INVALID_CHARS,
			dirname, -1, dirname_w, SUZR_ARRSIZEOF(dirname_w));
	if (conv_ret == 0)
		return suzr_io_win_last_error();

	wchar_t search[MAX_PATH];
	wcsncpy_s(search, SUZR_ARRSIZEOF(search), dirname_w, MAX_PATH);
	wcscat_s(search, SUZR_ARRSIZEOF(search), L"\\*");

	HANDLE hfind = FindFirstFileW(search, &ffd);
	if (hfind == INVALID_HANDLE_VALUE)
		return suzr_io_win_last_error();

	wchar_t oldpwd_buf[MAX_PATH];
	DWORD pwd_result = GetCurrentDirectoryW(SUZR_ARRSIZEOF(oldpwd_buf), oldpwd_buf);
	if (!pwd_result) {
		ret = suzr_io_win_last_error();
		goto exit_hfind;
	}

	BOOL cwd_result = SetCurrentDirectoryW(dirname_w);
	assert(cwd_result);
	if (!cwd_result) {
		ret = suzr_io_win_last_error();
		goto exit_hfind;
	}

	do {
		if (wcscmp(ffd.cFileName, L".") == 0)
			continue;
		if (wcscmp(ffd.cFileName, L"..") == 0)
			continue;

		char fullpath_buf[MAX_PATH];
		wchar_t fullpath_buf_w[MAX_PATH];
		DWORD fp_result = GetFullPathNameW(ffd.cFileName,
				SUZR_ARRSIZEOF(fullpath_buf_w), fullpath_buf_w, NULL);
		if (!fp_result) {
			ret = suzr_io_win_last_error();
			goto exit_oldpwd;
		}

		conv_ret = WideCharToMultiByte(CP_UTF8, 0,
				fullpath_buf_w, -1, fullpath_buf, sizeof(fullpath_buf),
				NULL, NULL);
		if (conv_ret == 0)
			return suzr_io_win_last_error();

		ret = 0;
		if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			if (recurse) {
				ret = suzr_io_recurse_directory(fullpath_buf, file_cb,
						recurse, baton);
			}
		} else {
			ret = file_cb(fullpath_buf, baton);
		}

		if (ret)
			goto exit_oldpwd;
	} while (FindNextFileW(hfind, &ffd) != 0);

exit_oldpwd:
	SetCurrentDirectoryW(oldpwd_buf);
exit_hfind:;
	BOOL clret = FindClose(hfind);
	if (!clret && !ret)
		return suzr_io_win_last_error();
	return ret;
}

SUZR_PUBLIC
int suzr_io_delete(const char *filename)
{
	wchar_t filename_w[MAX_PATH];
	int conv_ret = MultiByteToWideChar(CP_UTF8, MB_ERR_INVALID_CHARS,
			filename, -1, filename_w, SUZR_ARRSIZEOF(filename_w));
	if (conv_ret == 0)
		return suzr_io_win_last_error();

	BOOL success = DeleteFileW(filename_w);
	return success ? 0 : suzr_io_win_last_error();
}

SUZR_PUBLIC
int suzr_io_write(const char *filename,
               const void *buf,
               size_t buf_size)
{
	if (buf_size > DWORD_MAX)
		return E2BIG;

	wchar_t filename_w[MAX_PATH];
	int conv_ret = MultiByteToWideChar(CP_UTF8, MB_ERR_INVALID_CHARS,
			filename, -1, filename_w, SUZR_ARRSIZEOF(filename_w));
	if (conv_ret == 0)
		return suzr_io_win_last_error();

	HANDLE fd = CreateFileW(filename_w, GENERIC_WRITE, 0, NULL,
			CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (fd == INVALID_HANDLE_VALUE)
		return suzr_io_win_last_error();

	int ret = 0;
	BOOL wr_result = WriteFile(fd, buf, (DWORD)buf_size, NULL, NULL);
	if (!wr_result)
		ret = suzr_io_win_last_error();

	BOOL cl_result = CloseHandle(fd);
	if (!ret && !cl_result)
		return suzr_io_win_last_error();
	return ret;
}

SUZR_PUBLIC
int suzr_io_read(const char *filename, void **out, size_t *out_size)
{
	wchar_t filename_w[MAX_PATH];
	int conv_ret = MultiByteToWideChar(CP_UTF8, MB_ERR_INVALID_CHARS,
			filename, -1, filename_w, SUZR_ARRSIZEOF(filename_w));
	if (conv_ret == 0)
		return suzr_io_win_last_error();

	HANDLE fd = CreateFileW(filename_w, GENERIC_READ, 0, NULL,
			OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (fd == INVALID_HANDLE_VALUE)
		return suzr_io_win_last_error();

	int ret = 0;
	DWORD cur_size = 0;
	DWORD capacity = 4096;
	char *buf = malloc(capacity);
	if (!buf) {
		ret = ENOMEM;
		goto exit_close;
	}

	DWORD read_bytes;
	do {
		if (capacity - cur_size == 0) {
			DWORD old_cap = capacity;
			capacity *= 2;
			if (capacity < old_cap) {
				ret = E2BIG;
				goto exit_free;
			}
			buf = realloc(buf, capacity);
			if (!buf) {
				ret = ENOMEM;
				goto exit_close;
			}
		}
		BOOL rd_result = ReadFile(fd,
				&buf[cur_size], capacity - cur_size,
				&read_bytes, NULL);
		if (!rd_result) {
			ret = suzr_io_win_last_error();
			goto exit_free;
		}

		cur_size += read_bytes;
	} while (read_bytes);

	*out = buf;
	*out_size = cur_size;
	ret = 0;
	goto exit_close;

exit_free:
	free(buf);
exit_close:;
	BOOL cl_result = CloseHandle(fd);
	if (!ret && !cl_result)
		return suzr_io_win_last_error();
	return ret;
}

SUZR_PUBLIC
int suzr_io_get_user_file(char *buf, size_t buf_size, const char *filename)
{
	char env[MAX_PATH];
	wchar_t env_w[MAX_PATH];
	DWORD sz = GetEnvironmentVariableW(L"APPDATA", env_w, SUZR_ARRSIZEOF(env_w));
	if (!sz || sz >= MAX_PATH)
		return EIO;
	int conv_ret = WideCharToMultiByte(CP_UTF8, 0,
			env_w, -1, env, sizeof(env),
			NULL, NULL);
	if (conv_ret == 0)
		return suzr_io_win_last_error();

	size_t filename_len = strlen(filename);
	char *fn = malloc(filename_len + 1);
	if (!fn)
		return ENOMEM;

	for (size_t i = 0; i < filename_len; i++) {
		if (filename[i] == '/')
			fn[i] = '\\';
		else
			fn[i] = filename[i];
	}
	fn[filename_len] = '\0';

	int ret = snprintf(buf, buf_size, "%.*s\\" SUZR_USER_DIR_NAME "\\%s",
			(int)sz, env, fn);
	free(fn);

	if (ret < 0)
		return ret;
	if (ret >= buf_size)
		return ENOBUFS;
	return 0;
}

SUZR_PUBLIC
int suzr_io_ensure_user_dir(void)
{
	char user_dir[MAX_PATH];
	wchar_t user_dir_w[MAX_PATH];
	int ret;

	ret = suzr_io_get_user_file(user_dir, sizeof(user_dir), "");
	if (ret)
		return ret;

	int conv_ret = MultiByteToWideChar(CP_UTF8, MB_ERR_INVALID_CHARS,
			user_dir, -1, user_dir_w, SUZR_ARRSIZEOF(user_dir_w));
	if (conv_ret == 0)
		return suzr_io_win_last_error();

	ret = CreateDirectoryW(user_dir_w, NULL);
	if (!ret) {
		DWORD err = suzr_io_win_last_error();
		if (err == EEXIST)
			return 0;
		else
			return (int)err;
	}

	return 0;
}
