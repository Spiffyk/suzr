/*
 * Copyright (c) 2023 Oto Šťáva
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "stb_image.h"

#define SUZR_EXPORTS
#include "suzr.h"
#include "io.h"


// Config //////////////////////////////////////////////////////////////////////

SUZR_PUBLIC
int suzr_config_argparse(int argc, char **argv, struct suzr_config *out_config)
{
	bool version = false;
	bool recurse = false;
	unsigned int cap = 4;
	unsigned int num_files = 0;
	const char **files = malloc(cap * sizeof(*files));
	if (!files) abort();

	for (int i = 1; i < argc; i++) {
		const char *const arg = argv[i];

		/* Option processing */
		if (arg[0] == '-') {
			if (arg[1] == '\0') {
				fprintf(stderr, "Singular '-' argument not expected\n");
				return EINVAL;
			}

			/* Full-name (--) options */
			if (arg[1] == '-') {
				if (strcmp("version", &arg[2]) == 0) {
					version = true;
					continue;
				} else if (strcmp("recurse", &arg[2]) == 0) {
					recurse = true;
					continue;
				} else {
					fprintf(stderr, "Unknown argument '%s'\n", &arg[2]);
					return EINVAL;
				}
				continue;
			}

			/* Flag (-) options */
			for (int j = 1; arg[j]; j++) {
				switch (arg[j]) {
				case 'V':
					version = true;
					break;
				case 'r':
					recurse = true;
					break;
				default:
					fprintf(stderr, "Unknown flag '%c'\n", arg[j]);
					return EINVAL;
				}
			}
			continue;
		}

		/* File list processing */
		unsigned int ncap = cap;
		while (num_files > ncap)
			ncap *= 2;
		if (ncap > cap) {
			cap = ncap;
			files = realloc(files, cap * sizeof(*files));
			if (!files) abort();
		}

		files[num_files++] = arg;
	}

	*out_config = (struct suzr_config){
		.version = version,
		.recurse = recurse,
		.num_files = num_files,
		.files = files,
	};
	return 0;
}

SUZR_PUBLIC
void suzr_config_deinit(struct suzr_config *config)
{
	if (!config)
		return;
	free(config->files);
}

SUZR_PUBLIC
bool suzr_str_ends_with(const char *str, size_t str_buflen, const char *suffix)
{
	size_t str_len = strnlen(str, str_buflen);
	size_t suffix_len = strlen(suffix);

	if (suffix_len == 0)
		return true;
	if (str_len == 0)
		return (suffix_len == 0);

	size_t suf_i = suffix_len - 1;
	size_t str_i = str_len - 1;

	for (;;) {
		if (str[str_i] != suffix[suf_i])
			return false;

		if (str_i == 0)
			return (suf_i == 0);
		if (suf_i == 0)
			return true;

		str_i--;
		suf_i--;
	}
}

SUZR_PUBLIC
int suzr_str_replace_suffix(char *str, size_t str_buflen,
                            const char *old_suffix, const char *new_suffix)
{
	size_t str_len = strnlen(str, str_buflen);
	size_t old_suffix_len = strlen(old_suffix);

	if (old_suffix_len == 0)
		return EINVAL;
	if (str_len == 0)
		return 0;

	size_t old_suf_i = old_suffix_len - 1;
	size_t str_i = str_len - 1;

	for (;;) {
		if (str[str_i] != old_suffix[old_suf_i])
			return ERANGE;

		if (str_i == 0)
			break;
		if (old_suf_i == 0)
			break;

		str_i--;
		old_suf_i--;
	}

	size_t new_suffix_len = strlen(new_suffix);
	if (str_i + new_suffix_len > str_buflen)
		return ENOBUFS;

	memcpy(&str[str_i], new_suffix, new_suffix_len);
	str[str_i + new_suffix_len] = '\0';
	return 0;
}

SUZR_PUBLIC
void suzr_print_version(void)
{
	puts("suzr " SUZR_VERSION);
}


// Recursion callbacks /////////////////////////////////////////////////////////

#define BMP_HEADER_SIZE 14
#define DIB_HEADER_SIZE 124

struct wrbuf {
	unsigned char *buf;
	size_t size;
	size_t cur;
};

static struct wrbuf wrbuf_alloc(size_t size)
{
	unsigned char *buf = malloc(size);
	if (!buf) abort();

	return (struct wrbuf){
		.buf = buf,
		.size = size,
		.cur = 0
	};
}

static void wrbuf_write(struct wrbuf *wrbuf,
                        const void *x, size_t n)
{
	if (wrbuf->cur + n > wrbuf->size)
		abort();
	memcpy(&wrbuf->buf[wrbuf->cur], x, n);
	wrbuf->cur += n;
}

static void wrbuf_zeroes(struct wrbuf *wrbuf, size_t n)
{
	if (wrbuf->cur + n > wrbuf->size)
		abort();
	memset(&wrbuf->buf[wrbuf->cur], 0, n);
	wrbuf->cur += n;
}

static void wrbuf_free(struct wrbuf *wrbuf)
{
	free(wrbuf->buf);
	wrbuf->size = 0;
	wrbuf->cur = 0;
}

static int write_bmp(int width, int height, int comps,
                     const unsigned char *data,
                     const char *out_filename)
{
	static const uint32_t bmp_header_size = BMP_HEADER_SIZE;
	static const uint32_t dib_header_size = DIB_HEADER_SIZE;

	static const uint8_t signature[2] = "BM";
	static const uint32_t pix_offset = BMP_HEADER_SIZE + DIB_HEADER_SIZE;
	const size_t total_pix_bytes = (size_t)width * (size_t)height * 4;
	const uint32_t total_size = pix_offset + total_pix_bytes;

	const int32_t fwidth = width;
	const int32_t fheight = -height;
	static const uint16_t planes = 1;
	static const uint16_t bit_count = 32;
	static const uint32_t cs_srgb = 0x73524742;
	static const uint32_t intent = 2;

	static const int32_t compression = 3;
	static const uint8_t r_mask[4] = { 0xFF, 0x00, 0x00, 0x00 };
	static const uint8_t g_mask[4] = { 0x00, 0xFF, 0x00, 0x00 };
	static const uint8_t b_mask[4] = { 0x00, 0x00, 0xFF, 0x00 };
	static const uint8_t a_mask[4] = { 0x00, 0x00, 0x00, 0xFF };

	int ret = 0;
	struct wrbuf wb = wrbuf_alloc(total_size);

	/* BMP header */
	wrbuf_write(&wb, signature, sizeof(signature));
	wrbuf_write(&wb, &total_size, sizeof(total_size));
	wrbuf_zeroes(&wb, 2); // reserved
	wrbuf_zeroes(&wb, 2); // reserved
	wrbuf_write(&wb, &pix_offset, sizeof(pix_offset));
	assert(wb.cur == bmp_header_size);

	/* DIB header (BITMAPV5HEADER) */
	wrbuf_write(&wb, &dib_header_size, sizeof(dib_header_size));
	wrbuf_write(&wb, &fwidth, sizeof(fwidth));
	wrbuf_write(&wb, &fheight, sizeof(fheight));
	wrbuf_write(&wb, &planes, sizeof(planes));
	wrbuf_write(&wb, &bit_count, sizeof(bit_count));
	wrbuf_write(&wb, &compression, sizeof(compression));
	wrbuf_zeroes(&wb, sizeof(uint32_t)); // size image
	wrbuf_zeroes(&wb, sizeof(uint32_t)); // x pels per meter
	wrbuf_zeroes(&wb, sizeof(uint32_t)); // y pels per meter
	wrbuf_zeroes(&wb, sizeof(uint32_t)); // clr used
	wrbuf_zeroes(&wb, sizeof(uint32_t)); // clr important
	wrbuf_write(&wb, r_mask, sizeof(r_mask));
	wrbuf_write(&wb, g_mask, sizeof(g_mask));
	wrbuf_write(&wb, b_mask, sizeof(b_mask));
	wrbuf_write(&wb, a_mask, sizeof(a_mask));
	wrbuf_write(&wb, &cs_srgb, sizeof(cs_srgb));
	wrbuf_zeroes(&wb, sizeof(uint32_t) * 9); // endpoints
	wrbuf_zeroes(&wb, sizeof(uint32_t)); // gamma red
	wrbuf_zeroes(&wb, sizeof(uint32_t)); // gamma green
	wrbuf_zeroes(&wb, sizeof(uint32_t)); // gamma blue
	wrbuf_write(&wb, &intent, sizeof(intent));
	wrbuf_zeroes(&wb, sizeof(uint32_t)); // profile data
	wrbuf_zeroes(&wb, sizeof(uint32_t)); // profile data size
	wrbuf_zeroes(&wb, sizeof(uint32_t)); // reserved
	assert(wb.cur == pix_offset);

	/* Image data */
	uint8_t pixbuf[4] = {0};
	size_t cur = 0;
	while ((cur) < total_pix_bytes) {
		for (size_t i = 0; i < comps; i++)
			pixbuf[i] = data[cur + i];
		wrbuf_write(&wb, pixbuf, sizeof(pixbuf));
		cur += comps;
	}

	assert(wb.cur == wb.size);

	ret = suzr_io_write(out_filename, wb.buf, wb.size);
	wrbuf_free(&wb);
	return ret;
}

SUZR_PUBLIC
int suzr_gen_func(const char *in_filename, void *baton)
{
	struct suzr_stats *stats = baton;
	if (!suzr_str_ends_with(in_filename, strlen(in_filename), SUZR_IN_SUFFIX))
		return 0;

	char out_filename[512];
	strncpy(out_filename, in_filename, sizeof(out_filename));
	int ret = suzr_str_replace_suffix(out_filename, sizeof(out_filename),
			SUZR_IN_SUFFIX, SUZR_OUT_SUFFIX);
	if (ret)
		return ret;

	void* raw;
	size_t raw_size;
	ret = suzr_io_read(in_filename, &raw, &raw_size);
	if (ret)
		return ret;

	int width, height, comps;
	unsigned char *data = stbi_load_from_memory(raw, raw_size, &width, &height, &comps, 0);
	free(raw);
	if (!data)
		return 0;
	ret = write_bmp(width, height, comps, data, out_filename);
	if (ret) {
		fprintf(stderr, "Error while writing BMP\n");
		ret = EIO;
	}
	if (stats)
		stats->files_modified++;

	stbi_image_free(data);

	return ret;
}

SUZR_PUBLIC
int suzr_clean_func(const char *filename, void *baton)
{
	struct suzr_stats *stats = baton;
	if (!suzr_str_ends_with(filename, strlen(filename), SUZR_OUT_SUFFIX))
		return 0;

	suzr_io_delete(filename);
	if (stats)
		stats->files_modified++;
	return 0;
}
