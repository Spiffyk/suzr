# Suzanne's PSD recursor - NEWS

## suzr 1.2

**Upcoming**

- **GUI**: The browse button now says `Pick directory...` rather than just `...`
- **GUI, Windows**: Use the built-in Segoe UI font for nicer rendering
- **GUI, Linux**: Make some puny effort to use an available font
  - _Not sure if this will work anywhere other than Arch and I don't care
    anymore. Doing GUI stuff on Linux is just a pain. **You** can figure it
    out..._

## suzr 1.1

**Released**: 2024-09-21

- **CLI**: Add option to print the version
- **GUI**: Display version in the window title
- **GUI**: Replace radio buttons with just two action buttons
- **Linux**: Remove separation of the common functionality into a library and
  link it into the actual executables (gets rid of headaches)
- **Windows**: Proper support for international alphabets / wide-chars


## suzr 1.0

**Released**: 2023-09-23

- Initial version
