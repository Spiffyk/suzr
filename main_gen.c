/*
 * Copyright (c) 2023 Oto Šťáva
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <string.h>

#include "suzr.h"
#include "io.h"

int main(int argc, char **argv)
{
	struct suzr_config config;
	int ret = suzr_config_argparse(argc, argv, &config);
	if (ret)
		return 1;
	if (config.version) {
		suzr_print_version();
		return 0;
	}

	if (config.num_files == 0) {
		fprintf(stderr, "No directories given\n");
		return 1;
	}

	for (unsigned int i = 0; i < config.num_files; i++) {
		int ret = suzr_io_recurse_directory(config.files[i], suzr_gen_func,
				config.recurse, NULL);
		if (ret) {
			fprintf(stderr, "Error %d (%s) during recursion!\n",
					ret, strerror(ret));
			return 1;
		}
	}

	return 0;
}
